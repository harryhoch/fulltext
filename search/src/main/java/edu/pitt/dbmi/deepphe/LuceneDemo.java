package edu.pitt.dbmi.deepphe;

import java.lang.Exception;
import java.lang.System;
import java.nio.file.Path;
import java.nio.file.Paths;

import java.security.KeyStore;
import java.util.List;
import java.util.ArrayDeque;
import java.util.HashMap;
import java.util.Set;
import java.util.HashSet;
import java.util.Collection;
import java.util.Comparator;
import java.util.Map.Entry;
import java.util.Collections;
import java.util.ArrayList;


import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.index.IndexableField;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.index.Terms;
import org.apache.lucene.index.FieldInfo;
import org.apache.lucene.index.FieldInfos;
import org.apache.lucene.index.LeafReader;
import org.apache.lucene.index.LeafReaderContext;
import org.apache.lucene.index.IndexReaderContext;;
import org.apache.lucene.search.highlight.SimpleHTMLFormatter;
import org.apache.lucene.search.highlight.Formatter;
import org.apache.lucene.search.highlight.SimpleSpanFragmenter;
import org.apache.lucene.search.highlight.Fragmenter;
import org.apache.lucene.search.highlight.Highlighter;
import org.apache.lucene.search.highlight.QueryScorer;
import org.apache.lucene.index.Fields;
import org.apache.lucene.index.TermsEnum;
import org.apache.lucene.util.BytesRef;
import org.apache.lucene.index.PostingsEnum;
import org.apache.lucene.search.highlight.TokenGroup;
import org.apache.lucene.search.highlight.TextFragment;
import org.apache.lucene.search.spans.SpanTermQuery;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.spans.SpanWeight;
import org.apache.lucene.search.spans.SpanWeight.Postings;
import org.apache.lucene.search.spans.Spans;
import org.apache.lucene.search.TotalHits;
import org.apache.lucene.search.ScoreMode;
import org.apache.lucene.search.Matches;
import org.apache.lucene.search.MatchesIterator;
import org.apache.lucene.index.FieldInfos;
/**
 * Hello world!
 *
 */


public class LuceneDemo {


    public static boolean verbose = false;

    //private static String lucenePath="/Users/harry/Documents/research/projects/deeppheno/dev/v2-3.5/fulltext";
   // private static String lucenePath = "/Users/harry/Documents/research/projects/deepphe/v2/fulltext";
    private static String indexPath="/data/index";


    public static void main(String[] args) throws Exception {
        Path currentRelativePath = Paths.get("");
        String cwd = currentRelativePath.toAbsolutePath().toString();
        System.out.println("directory is " + cwd);
        System.out.println("index is " + indexPath);

        // open the search
        //in directory
        // /Users/harry/Documents/research/projects/deeppheno/dev/v2-3.5/output/output_graph/deepphe.db/index/lucene/node/name_index

        IndexReader reader = DirectoryReader.open(FSDirectory.open(Paths.get(cwd,indexPath)));

        getContexts(reader);


        getDocCount(reader, "contents");
        getDocCount(reader, "notes");


        getLeaves(reader);

        // get field names.      https://stackoverflow.com/questions/20980466/lucene-reading-all-field-names-that-are-stored
        getFields(reader);

        System.out.println("# of documents " + reader.maxDoc());

        if (verbose == true) {
            showDocs(reader);
        }
        doSearch(reader);

        doSpanTermSearch(reader);


        getAllTerms(reader,"contents");

    }


    public static void doSearch(IndexReader reader) throws Exception {

        String term = "sing";
        System.out.println("-=-=-=-=-=-=-=-=-=");
        System.out.println("running a search.. for " + term);
        IndexSearcher searcher = new IndexSearcher(reader);
        StandardAnalyzer analyzer = new StandardAnalyzer();

        // created analyzer. now, let's run a search.
        QueryParser parser = new QueryParser("contents", analyzer);

        Query query = parser.parse(term);
        TopDocs results = searcher.search(query, 100);

        TotalHits numTotalHits = results.totalHits;
        System.out.println("# of results..." + numTotalHits.value);

        // taken from
        // https://howtodoinjava.com/lucene/lucene-search-highlight-example/#search-highlight
        // and http://makble.com/how-to-do-lucene-search-highlight-example

        //Uses HTML  tags to highlight the searched terms
        //Formatter formatter = new SimpleHTMLFormatter("","");
        Formatter formatter = new NoOpFormatter();

        //It scores text fragments by the number of unique query terms found
        //Basically the matching score in layman terms
        QueryScorer scorer = new QueryScorer(query);

        //used to markup highlighted terms found in the best sections of a text. Can use the html formatter if needed.
        Highlighter highlighter = new Highlighter(formatter, scorer);

        //It breaks text up into same-size texts but does not split up spans
        Fragmenter fragmenter = new SimpleSpanFragmenter(scorer, 10);

        //breaks text up into same-size fragments with no concerns over spotting sentence boundaries.
        //Fragmenter fragmenter = new SimpleFragmenter(10);

        //set fragmenter to highlighter
        highlighter.setTextFragmenter(fragmenter);


        ScoreDoc[] searchResults = results.scoreDocs;

        for (int i = 0; i < searchResults.length; i++) {
            ScoreDoc sd = searchResults[i];
            showSearchResult(searcher, analyzer, highlighter, i, sd);
        }
        System.out.println("-=-=-=-=-=-=-=-=-=");
    }

    private static void showSearchResult(IndexSearcher searcher, StandardAnalyzer analyzer,
                                         Highlighter high, int i, ScoreDoc sd) throws Exception {
        System.out.println(" - - - - - -");
        System.out.println("hit " + i + " score: " + sd.score);
        Document doc = searcher.doc(sd.doc);

        System.out.println("document id is " + sd.doc);

        IndexReader reader = searcher.getIndexReader();
        String path = doc.get("path");
        System.out.println("path is " + path);
        String modified = doc.get("modified");
        System.out.println("modified " + modified);
        String contents = doc.get("contents");


        //TokenStream stream = TokenSources.getAnyTokenStream(reader, sd.doc, "contents", analyzer);
        //TokenStream stream = TokenSources.getTokenStream("contents",contents,analyzer);
        Fields fields = reader.getTermVectors(sd.doc);

        Terms vector = fields.terms("contents");
        printTermsDetails(vector);

        TokenStream stream = analyzer.tokenStream("contents", contents);
        //TokenStream stream = new TokenStreamFromTermVector(vector,0);
        //TokenStream stream = TokenSources.getTermVectorTokenStreamOrNull("contents",fields,0);
        //Get highlighted text fragments
        TextFragment[] frags = high.getBestTextFragments(stream, contents, true, 10);
        for (TextFragment frag : frags) {
            System.out.println("FRAGMENTS=======================");
            System.out.println("#" + frag.getFragNum() + ", score: " + frag.getScore() + " " + frag.toString());
        }

        System.out.println(contents);
    }


    public static void printTermsDetails(Terms vector) throws Exception {

        System.out.println("..........");
        System.out.println("what's in the term vector");
        System.out.println(" # of docs is " + vector.getDocCount());
        System.out.println("..min .." + vector.getMin());
        System.out.println("..max.." + vector.getMax());
        System.out.println("...hasFreqs.." + vector.hasFreqs());
        System.out.println("... hasOffset..." + vector.hasOffsets());

        TermsEnum iter = vector.iterator();
        BytesRef b = iter.next();
        while ((b = iter.next()) != null) {
            System.out.println("*******");
            System.out.println("TERM ..." + b.utf8ToString());
            System.out.println("POSTINGS...");
            PostingsEnum postings = null;
            postings = iter.postings(postings, PostingsEnum.ALL);
            int doc;
            while ((doc = postings.nextDoc()) != PostingsEnum.NO_MORE_DOCS) {
                int freq = postings.freq();
                System.out.println("occurs " + freq + " times");
                for (int i = 0; i < freq; i++) {
                    System.out.println(postings.nextPosition() + " " + postings.startOffset() +
                            " " + postings.endOffset());
                }
            }
        }
    }

   /* public static void printFieldDetails(Fields fields) throws Exception {
        System.out.println("-----fields----");
        Iterator<String> iter = fields.iterator();
        while (iter.hasNext()) {
            String field = iter.next();
            System.out.println("Field ..."+field);
            Terms fterms =   fields.terms(field);
            if (fterms.hasPositions()) {
                System.out.println("has positions");
                TermsEnum termsEnum = fterms.iterator();
                PostingsEnum postings = null;

                while (termsEnum.next() != null) {
                    postings = termsEnum.postings(postings, PostingsEnum.ALL);
                    int doc;
                    while ((doc = postings.nextDoc()) != PostingsEnum.NO_MORE_DOCS) {
                        if (postings.freq() >1) {
                            System.out.println(postings.nextPosition() + " " + postings.freq() + " " + postings.getPayload() + " " + postings.startOffset() + " " + postings.endOffset());
                        }
                    }
                }
            }
        }
        System.out.println("------fields----");
    }*/


    public static void getFields(IndexReader reader) throws Exception {


        System.out.println("***** getFields ***** ");
        HashMap<String, Integer> allFields = new HashMap<String, Integer>();


        int maxdoc = reader.maxDoc();
        for (int i = 0; i < maxdoc; i++) {
            Document doc = reader.document(i);
            List<IndexableField> fields = doc.getFields();
            for (IndexableField f : fields) {
                String field = f.name();
                if (!allFields.containsKey(field)) {
                    allFields.put(field, 0);
                }
                int count = allFields.get(field) + 1;
                allFields.put(field, count);
            }
        }
        System.out.println("indexed fields...");
        Set<String> foundFields = allFields.keySet();
        for (String s : foundFields) {
            System.out.println(s);
        }
        System.out.println("done with indexed fields");
        System.out.println("***** getFields ***** ");
    }

    public static void showDocs(IndexReader reader) throws Exception {
        for (int i = 0; i < reader.maxDoc(); i++) {
            showDoc(reader, i);
        }

    }

    public static void showDoc(IndexReader reader, int i) throws Exception {
        Document doc = reader.document(i);
        System.out.println("=======");
        System.out.println("Document " + i);
        System.out.println("name is " + doc.get("name"));
        System.out.println(doc.toString());
        List<IndexableField> fields = doc.getFields();
        for (IndexableField f : fields) {
            String field = f.name();
            System.out.println("Field: " + field);
            String[] fvals = doc.getValues(field);
            for (String fv : fvals) {
                System.out.println("..." + fv);
            }
        }
    }

    public static void getDocCount(IndexReader ir, String field) throws Exception {
        int count = ir.getDocCount(field);
        System.out.println("document count for '" + field + "' is " + count);
    }

    public static void getLeaves(IndexReader reader) throws Exception {

        System.out.println("..........");
        System.out.println("leaf field names..");

        HashMap<String, Integer> allFields = new HashMap<String, Integer>();

        for (LeafReaderContext rc : reader.leaves()) {
            System.out.println("looking at one leaf");
            LeafReader ar = rc.reader();
            FieldInfos fis = ar.getFieldInfos();
            for (FieldInfo fi : fis) {
                String name = fi.name;
                if (!allFields.containsKey(name)) {
                    allFields.put(name, 0);
                }
                int count = allFields.get(name) + 1;
                allFields.put(name, count);
            }
        };

        allFields.forEach((k,v)  -> System.out.println("context.."+k+", count "+v));
        System.out.println("..........");
    }


    static void getContexts(IndexReader reader) throws Exception {
        IndexReaderContext context = reader.getContext();
        System.out.println("*+*+*+*+*+*+");
        ArrayDeque<IndexReaderContext> contexts = new ArrayDeque<IndexReaderContext>();
        contexts.add(context);

        while (!contexts.isEmpty()) {
            IndexReaderContext irc = contexts.poll();
            if (irc.isTopLevel == true) {
                System.out.println("top level...");
            } else {
                System.out.println("not top level");
            }
            List<IndexReaderContext> children = irc.children();
            if (children != null) {
                System.out.println("adding children");
                for (IndexReaderContext c : children) {
                    contexts.add(c);
                }
            }
        }
        System.out.println("*+*+*+*+*+*+");
    }


    public static void doSpanTermSearch(IndexReader reader) throws Exception {

        String term = "sing";
        System.out.println("-=-=-=-=-=-=-=-=-=");
        System.out.println("running a search.. for " + term);
        IndexSearcher searcher = new IndexSearcher(reader);
        StandardAnalyzer analyzer = new StandardAnalyzer();

        // created analyzer. now, let's run a search.

        SpanTermQuery stq = new SpanTermQuery(new Term("contents", term));
        //TopDocs results = searcher.search(stq, 10);
        SpanWeight spanWeight = stq.createWeight(searcher, ScoreMode.COMPLETE, 1.0f);

        /*https://stackoverflow.com/questions/32385887/how-to-get-the-matching-spans-of-a-span-term-query-in-lucene-5*/
        /*ok. how to get from indexreader to LeafReader) */
        Spans spans = spanWeight.getSpans(reader.leaves().get(0),SpanWeight.Postings.POSITIONS);

        // from lucene-8.0.0/core/src/test/org/apache/lucene/search/TestPositionIncrement.java
        // each span has a doc id, but then if I get matches, for that Id, I get all matches, which is redundant
        // so, collect all doc ids and then print spans.


        // get unique doc ids
        HashSet<Integer> docs = new HashSet<Integer>();
        while (spans.nextDoc() != Spans.NO_MORE_DOCS) {
            docs.add(spans.docID());
        }

        System.out.println("=-=-=-=   LOOKING at MATCHES -=-=-=-=-=");

        for (int doc: docs) {
            Matches matches = spanWeight.matches(reader.leaves().get(0),doc);
            MatchesIterator miter = matches.getMatches("contents");
            while (miter.next()) {
                System.out.println("document "+doc+", start pos " + miter.startPosition() + ", end pos " + miter.endPosition() +
                        " start offset " + miter.startOffset() + ", end offset " + miter.endOffset());
            }
        }

        /*while (spans.nextDoc() != Spans.NO_MORE_DOCS) {
            while (spans.nextStartPosition() != Spans.NO_MORE_POSITIONS) {
                Document doc = reader.document(spans.docID());
                //  System.out.println("...title is "+doc.get("path"));
                System.out.println("doc " + spans.docID() + ": span " + spans.startPosition()
                        + " to " + spans.endPosition() + " ..path..." + doc.get("path"));


                System.out.println("=-=-=-=   LOOKING at MATCHES -=-=-=-=-=");


                Matches matches = spanWeight.matches(reader.leaves().get(0),spans.docID());
                MatchesIterator miter = matches.getMatches("contents");
                while (miter.next()) {
                        System.out.println("start pos "+miter.startPosition()+", end pos "+miter.endPosition()+
                                " start offset "+miter.startOffset()+", end offset "+miter.endOffset());
                    
                }
            }
        }*/

    }

    static void getAllTerms(IndexReader reader,String field) throws Exception {

        LeafReaderContext  lrc = reader.leaves().get(0);
        LeafReader lreader = lrc.reader();
        
        System.out.println("getting all terms..");
        Collection<String> fields = FieldInfos.getIndexedFields(reader);
        System.out.println("total term frequency for "+field+" is "+lreader.getSumTotalTermFreq(field));
        System.out.println("total sum document Freq frequency for "+field+" is "+lreader.getSumDocFreq(field));
        Terms terms = lreader.terms(field);

        int totalCount = 0;
        int docFreq= 0;
        HashMap<String,Long> termCounts = new HashMap<String,Long>();
        HashMap<String,Long> docFreqs = new HashMap<String,Long>();
        TermsEnum titer = terms.iterator();

        BytesRef b;
        while ((b = titer.next()) != null) {
            String termName = b.utf8ToString();
            long freq = titer.totalTermFreq();
            totalCount += freq;
            long df = titer.docFreq();
            docFreq +=  df;
            termCounts.put(termName,freq);
            docFreqs.put(termName,df);
        }
        System.out.println("total count..."+totalCount);
        System.out.println("printing overalll freqs -- # of times each term found overall.");
        Comparator<Entry<String,Long>>  tComparator =  new Comparator<Entry<String,Long>>() {
            @Override
            public int compare(Entry<String,Long> e1, Entry<String,Long> e2) {
                long  diff =     e2.getValue()-e1.getValue();
                return (int) diff;
            }
        };
        List  <Entry<String, Long>> tclist = new ArrayList<Entry<String, Long>>(termCounts.entrySet());

        Collections.sort(tclist, tComparator);
        for (Entry<String,Long> ent: tclist) {
            System.out.println(ent.getKey()+" "+ent.getValue());
        }

        List  <Entry<String, Long>> dflist = new ArrayList<Entry<String, Long>>(docFreqs.entrySet());

        System.out.println("# of docs each term found in...");
        Collections.sort(dflist, tComparator);
        for (Entry<String,Long> ent: dflist) {
            System.out.println(ent.getKey()+" "+ent.getValue());
        }


        boolean countChecks = (lreader.getSumTotalTermFreq("contents") == totalCount);
        System.out.println("countchecks "+countChecks);

        boolean countDocFreq = (lreader.getSumDocFreq("contents") == docFreq);
        System.out.println("count doc "+countDocFreq);

        System.out.println("# of documents.."+reader.numDocs());
        System.out.println("total term frequency for "+field+" is "+lreader.getSumTotalTermFreq(field));
        System.out.println("total sum document Freq frequency for "+field+" is "+lreader.getSumDocFreq(field));
        float termsperdoc = ((float) lreader.getSumDocFreq(field))/((float)lreader.numDocs());
        System.out.println("# of terms/doc is "+termsperdoc);

    }
}



 /*TermsEnum iter = vector.iterator();
    BytesRef b = iter.next();
        while ((b = iter.next()) != null) {
                System.out.println("*******");
                System.out.println("TERM ..." + b.utf8ToString());
                System.out.println("POSTINGS...");
                PostingsEnum postings = null;
                postings = iter.postings(postings, PostingsEnum.ALL);
                int doc;
                while ((doc = postings.nextDoc()) != PostingsEnum.NO_MORE_DOCS) {
                int freq = postings.freq();
                System.out.println("occurs " + freq + " times");
                for (int i = 0; i < freq; i++) {
        System.out.println(postings.nextPosition() + " " + postings.startOffset() +
        " " + postings.endOffset());
        }
        }      */


 /*while ((doc = postings.nextDoc()) != PostingsEnum.NO_MORE_DOCS) {
         int freq = postings.freq();
         System.out.println("occurs " + freq + " times");
         for (int i = 0; i < freq; i++) {
        System.out.println(postings.nextPosition() + " " + postings.startOffset() +
        " " + postings.endOffset());
        }
        }
        }*/



class NoOpFormatter implements Formatter {
    public String highlightTerm(String originalText, TokenGroup tokenGroup) {
        return originalText;
    }
}